<?php
/**
 *  (c) 2014 - Abricot theme (nicolabricot.com)
 */

class OC_Theme {

    private $myEntity;
    private $myName;
    private $myTitle;
    private $myBaseUrl;
    private $mySlogan;
    private $myLogoClaim;

    function __construct() {
        /* company name, used for footers and copyright notices */
        $this->myEntity = "Framasoft"; 
        /* short name, used when referring to the software, footer in e-mail signature or sender */
        $this->myName = "Framadrive"; 
        /* can be a longer name, for titles */
        $this->myTitle = "Framadrive"; 
        $this->myBaseUrl = "https://framadrive.org";
        /* used in login footer */
        $this->mySlogan = "Hébergez vos documents chiffrés en ligne";
        $this->myLogoClaim = "";
    }

    public function getBaseUrl() {
        return $this->myBaseUrl;
    }
    public function getTitle() {
        return $this->myTitle;
    }
    public function getName() {
        return $this->myName;
    }
    public function getEntity() {
        return $this->myEntity;
    }
    public function getSlogan() {
        return $this->mySlogan;
    }
    public function getLogoClaim() {
        return $this->myLogoClaim;
    }
    public function getShortFooter() {
        $footer = "<a href=\"". $this->getBaseUrl() . "\" target=\"_blank\">" . $this->getEntity() . "</a>" . ' &middot; ' . $this->getSlogan();
        return $footer;
    }
}
